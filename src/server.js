//Import necessary libraries
import express from 'express';
import config from '../config';
import dataJson from '../public/data';
//Create server by express
const app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//Serve static HTML file
app.get('/', function (req, res) {
    res.send('<h1>Hello from server</h1>');
});


app.get('/data', function (req, res) {
    res.json(dataJson);
});

//Listening on port
app.listen(config.port, err => {
    if(err) console.error(err);
    console.log(`Server is running on port ${config.port}...`);
});